$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function ajaxAction(url) {
    $('#overlay').show();
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            location.reload();
        }
    });
}

function ajaxActionDelete(url, redirect) {
    $('#overlay').show();
    $.ajax({
        url: url,
        type: 'DELETE',
        dataType: 'JSON',
        success: function (data) {
            $('#overlay').hide();
            document.location = redirect;
        }
    });
}

//function test(json) {
//    var time = 5 * 1000;
//    blockUI('.....');
//    for (var i = 0; i < json.length; i++) {
//        setTimeout(complete, time, json[i], 0);
//        time += (1000 * json[i].duration);
//    }
//    setTimeout(function () {
//        $.unblockUI();
//        location.reload();
//    }, time);
//}

function ajaxActionSort(url) {
    $('#overlay').show();
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            $('#overlay').hide();
        }
    });
}

// Return a helper with preserved width of cells
var fixHelper = function (e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
};

//jquery-ui components
$(function () {
    $("#sortable tbody").sortable({
        helper: fixHelper,
        cancel: '',
        update: function (event, ui) {
            console.log(ui);
            var res = ui.item[0].className.split(' ');
            var sortedIDs = $("#sortable tbody").sortable("toArray");
            ajaxActionSort('/' + res[0] + 's/sort/' + sortedIDs);
        }
    });
    $("#sortable tbody").disableSelection();

    //$(".droppable").droppable({
    //    drop: function (event, ui) {
    //
    //
    //        console.log('Dropped..update subtask ' + ui.draggable[0].id + ' with set id:' + $(this)[0].id);
    //    }
    //});
    //$(document).tooltip();
});

