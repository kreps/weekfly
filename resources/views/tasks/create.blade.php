@extends('app')
@section('content')
    {{--<h3><span class="badge">{{$goal->priority}}</span> {{$goal->name}}</h3>--}}
    {!! Form::model($taskNew = new \App\Task(),['url' => "tasks", 'role' => 'form']) !!}
    @include('tasks.form',['submitButtonText'=>'New task' ])
    {!! Form::close() !!}
    @include('errors.list')
@stop