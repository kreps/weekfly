@extends('app')

@section('content')
    {!! Form::model($task, ['method' => 'PATCH','url' => 'tasks/'.$task->id]) !!}
    @include('tasks.form',['submitButtonText'=>'Update'])
    {!! Form::close() !!}
    @include('errors.list')
@stop