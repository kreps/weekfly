<?php
use App\Goal;
$goals = Goal::all()->first();
?>
@extends('app')
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div style="margin-right: 10px; float: left;">
        <table id="sortable">
            <tbody>
            @foreach ($tasks as $task)
                @if(!$task->iscompleted)
                    <tr id="{{$task->id}}" class="task">
                        <td id="{{$task->id}}" onclick="document.location.href = '/tasks/{{$task->id}}'">
                            @include('partials.statusandname',['item' => $task])
                        </td>
                        <?php
                        $goal = Goal::find($task->goal_id);
                        ?>
                        <td class="actions">

                            @if(!$task->iscompleted)
                                @include('buttons.complete',['url'=>"/tasks/$task->id/complete",'tooltip'=>'Complete'])
                            @else
                                @include('buttons.uncomplete',['url'=>"/tasks/$task->id/uncomplete",'tooltip'=>'Uncomplete'])
                            @endif
                            <div class="btn-group">
                                <button type="button" class="black" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="/tasks/{{$task->id}}/edit">Edit</a></li>
                                    <li><a href="/tasks/{{$task->id}}/duplicate">Duplicate</a></li>
                                    <li><a href="/subtasks/create/{{$task->id}}">Add task</a></li>
                                    <li style="cursor: pointer"><a onclick="
                                                var y = confirm('Are you sure you want to delete?');
                                                if (y == true) ajaxActionDelete('/tasks/{{$task->id}}','/tasks/');
                                                ">Delete</a></li>
                                </ul>
                            </div>
                        </td>

                        <td align="right">
                            @if($goal)    <span class="label label-info" style="margin-left: 10px; cursor: pointer; color: #333;"
                                                onclick="document.location.href = '/goals/{{$goal->id}}'">{{$goal->name}}</span>
                            @endif
                        </td>


                        <td style="width:10px"></td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    @if($goals)
        <div class="description">
            <p>Create tasks for your goals. </p>

            <p>Task can be a workout, meating, event, todo item. </p>

            <p>Tasks can also be reoccuring. </p>
            @include('buttons.button',['text' => 'Create new task','url'=>'/tasks/create','class' => 'blackbutton'])
        </div>
    @endif
@stop

@section('footer')
    @include('partials.footer')
@stop