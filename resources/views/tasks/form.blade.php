<div class="form-group">
    {!! Form::label('goal_id','Goal:') !!}
    {!! Form::select('goal_id',$goals,$id?$id: null,['class' => 'form-control','style' => 'text-transform:uppercase;']) !!}
</div>

<div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::input('text','name',$task?$task->name: null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description','Description:') !!}
    {!! Form::textarea('description',$task?$task->description: null,['class' => 'form-control','size' => '30x5']) !!}
</div>

<div class="form-group">
    {!! Form::label('duedate','Due date:') !!}
    {!! Form::input('date','duedate',Carbon\Carbon::now()->format('Y-m-d'),['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority','Priority:') !!}
    {!! Form::select('priority',['A'=>'A','B'=>'B','C'=>'C'],$task?$task->priority: 'A',['class' => 'form-control','multiple']) !!}
</div>

<div class="form-group">
    {!! Form::label('duration','Duration:') !!}
    {!! Form::input('text','duration',$task? secondsToTime($task->duration):'1 h',['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText,['class' => 'btn btn-default form-control']) !!}
</div>

@section('footer')
@endsection