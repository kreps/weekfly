@extends('app')
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div style="float:left; padding-right: 10px">
        <table id="sortable">
            <thead>
            <tr class="task" id="{{ $task->id }}">
                <td>
                    @include('partials.statusandname',['item' => $task])
                </td>
                <td class="actions">
                    @if (!$task->iscompleted)
                        @include('buttons.complete',['url'=>"/tasks/$task->id/complete",'tooltip'=>'Complete task'])
                    @else
                        @include('buttons.uncomplete',['url'=>"/tasks/$task->id/uncomplete",'tooltip'=>'Uncomplete'])
                    @endif
                    @include('buttons.edit',['url' => "/tasks/$task->id/edit",'tooltip' => 'Edit task'])
                    @include('buttons.createSubTask',['url' => "/subtasks/create/$task->id",'tooltip' => 'Add sub task'])
                    @include('buttons.destroy',['url' => "/tasks/$task->id",'redirect'=>'/tasks/','tooltip' => 'Delete task'])
                </td>
                <td>
                    @if($goal)
                        <span class="label label-info" style="margin-right: 10px; cursor: pointer;"
                              onclick="document.location.href = '/goals/{{$task->goal_id}}'">{{ $goal->name }}</span>
                    @endif
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach($subtasks as $subtask)
                <tr id="{{$subtask->id}}" class="subtask">
                    <td>
                        @include('partials.statusandname',['item' => $subtask])
                    </td>
                    <td class="actions" colspan="2">
                        @if (!$subtask->iscompleted)
                            @include('buttons.complete',['url'=>"/subtasks/$subtask->id/complete",'tooltip'=>'Complete task'])
                        @else
                            @include('buttons.uncomplete',['url'=>"/subtasks/$subtask->id/uncomplete",'tooltip'=>'Uncomplete task'])
                        @endif
                        <div class="btn-group">
                            <button type="button" class="ligthgrey" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                <span class="glyphicon glyphicon-menu-hamburger"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="/subtasks/{{$subtask->id}}/edit">Edit</a></li>
                                <li style="cursor: pointer"><a onclick="
                                            var y = confirm('Are you sure you want to delete?');
                                            if (y == true) ajaxActionDelete('/subtasks/{{$subtask->id}}','/tasks/{{$task->id}}');
                                            ">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @if ($task->description)
        <div class="description">{{ $task->description }}</div>
    @endif
@stop

@section('footer')
    @include('partials.footer')
@stop


