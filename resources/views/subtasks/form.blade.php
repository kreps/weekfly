{!! Form::input('hidden','task_id',$id?$id:$subtask->task_id,['class' => 'form-control']) !!}

<div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::input('text','name',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description','Description:') !!}
    {!! Form::textarea('description',null,['class' => 'form-control','foo' => 'bar','size' => '30x5']) !!}
</div>

<div class="form-group">
    {!! Form::label('duedate','Due date:') !!}
    {!! Form::input('date','duedate',$subtask->duedate->format('Y-m-d'),['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority','Priority:') !!}
    {!! Form::select('priority',['A'=>'A','B'=>'B','C'=>'C'],$subtask->priority ? $subtask->priority :'A',['class' => 'form-control','multiple']) !!}
</div>

<div class="form-group">
    {!! Form::label('duration','Duration:') !!}
    {!! Form::input('text','duration',secondsToTime($subtask->duration),['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText,['class' => 'btn btn-default form-control']) !!}
</div>

@section('footer')
@endsection