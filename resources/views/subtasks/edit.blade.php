@extends('app')

@section('content')
    {!! Form::model($subtask, ['method' => 'PATCH','url' => 'subtasks/'.$subtask->id]) !!}
    @include('subtasks.form',['submitButtonText'=>'Update'])
    {!! Form::close() !!}
    @include('errors.list')
@stop