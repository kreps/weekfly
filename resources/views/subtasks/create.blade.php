@extends('app')
@section('content')
    {!! Form::model($subTaskNew = new \App\SubTask(),['url' => "subtasks"]) !!}
    @include('subtasks.form',['submitButtonText'=>'Create sub task' ])
    {!! Form::close() !!}
    @include('errors.list')
@stop