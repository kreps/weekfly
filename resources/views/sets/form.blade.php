<div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::input('text','name',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('task_id','Task id:') !!}
    {!! Form::input('text','task_id',$task->id,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority','Priority:') !!}
    {!! Form::select('priority',['A'=>'A','B'=>'B','C'=>'C'],'A',['class' => 'form-control','multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText,['class' => 'btn btn-default form-control']) !!}
</div>

@section('footer')
@endsection