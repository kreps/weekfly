@extends('app')
@section('content')
    {!! Form::model($newSet = new \App\Set(),['url' => "sets"]) !!}
    @include('sets.form',['submitButtonText'=>'Create set' ])
    {!! Form::close() !!}
    @include('errors.list')
@stop