<set id="{{$set->id}}" class="droppable">
    <table width="100%">
        <tr>
            <td style="
                            height: 40px;
                            border: 1px solid #ddd;
                            background-color: #ddd;
                            background-image: linear-gradient(#ddd, #9df);
                            text-transform: uppercase;
                            padding: 10px;
                            color: #333;">
                <span onclick="$('#setActions<?php echo $set->id; ?>').toggle();">{{$set->name}}</span>
                @include('sets.actions')
            </td>
        </tr>

        @foreach($set->subtasks as $subtask)
            <tr>
                <td>
                    <span class="list-group list-group-item" style="width: 200px">{{$subtask->name}}
                        <actions>
                            @if (!$task->iscompleted)
                                <span class="btn glyphicon glyphicon-ok" aria-hidden="true"
                                      onclick="ajaxAction('/tasks/{{$task->id}}/complete');"
                                      {{$ttp}} data-toggle="tooltip" title="Complete"></span>

                            @else

                                <span class="btn glyphicon glyphicon-ok" aria-hidden="true"
                                      onclick="ajaxAction('/tasks/{{$task->id}}/uncomplete');"
                                      {{$ttp}} data-toggle="tooltip" title="Uncomplete"></span>

                            @endif
                        </actions>
                    </span>
                </td>
            </tr>
        @endforeach
    </table>

</set>