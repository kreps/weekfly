<actions id="setActions{{$set->id}}">
    @include('buttons.createSubTask',['url' => "/subtasks/$task->id/create/$set->id",'tooltip' => 'Add sub task'])
    @include('buttons.destroy',['url' => "/sets/$set->id", 'tooltip'=>'Delete set' ])
</actions>