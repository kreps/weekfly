@extends('app')

@section('content')

    <div style="text-align: center">
        {{--<img src="/img/logo.png" style="width: 150px; height: 150px; float:left">--}}
        @include('buttons.button',['text' => 'Login','url'=>'/auth/login','class' => 'blackbutton'])
        @include('buttons.button',['text' => 'Create account','url'=>'/auth/register','class' => 'bluebutton'])
    </div>

@endsection
