@extends('app')
@section('content')
    {!! Form::model($newGoal = new \App\Goal(),['url' => "goals"]) !!}
    @include('goals.form',['submitButtonText'=>'Create' ])
    {!! Form::close() !!}
    @include('errors.list')
@stop