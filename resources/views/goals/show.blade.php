@extends('app')

@section('header')
    @include('partials.header')
@stop

@section('content')
    <table id="sortable" style="float:left; margin-right: 10px; margin-bottom: 5px;">
        <tr class="goal">
            <td>
                @include('partials.statusandname',['item' => $goal])
            </td>
            <td class="actions">
                @if(!$goal->iscompleted)
                    @include('buttons.complete',['url' => "/goals/$goal->id/complete", 'tooltip'=>'Complete goal' ])
                @else
                    @include('buttons.uncomplete',['url'=>"/goals/$goal->id/uncomplete",'tooltip'=>'Uncomplete goal'])
                @endif
                @include('buttons.edit',['url' => "/goals/$goal->id/edit", 'tooltip'=>'Edit goal' ])
                @include('buttons.createTask',['url' => "/tasks/create/$goal->id", 'tooltip'=>'Add task' ])
                @include('buttons.destroy',['url' => "/goals/$goal->id", 'redirect'=> '/goals/','tooltip'=>'Delete goal' ])
            </td>
        </tr>
        @foreach ($tasks as $task)
            <tr class="task" style="padding-top: 2px;">
                <td onclick="document.location.href = '/tasks/{{$task->id}}'">
                    @include('partials.statusandname',['item' => $task])
                </td>
                <td class="actions">
                    @if(!$task->iscompleted)
                        @include('buttons.complete',['url'=>"/tasks/$task->id/complete",'tooltip'=>'Complete'])
                    @else
                        @include('buttons.uncomplete',['url'=>"/tasks/$task->id/uncomplete",'tooltip'=>'Uncomplete'])
                    @endif
                    <div class="btn-group">
                        <button type="button" class="black" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="/tasks/{{$task->id}}/edit">Edit</a></li>
                            <li><a href="/subtasks/{{$task->id}}/create">Add task</a></li>
                            <li style="cursor: pointer"><a onclick="
                                        var y = confirm('Are you sure you want to delete?');
                                        if (y == true) ajaxActionDelete('/tasks/{{$task->id}}','/tasks/');
                                        ">Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
    <div class="description">
        <p>{{$goal->description}}</p>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop


