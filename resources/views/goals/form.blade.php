<div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::input('text','name',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description','Description:') !!}
    {!! Form::textarea('description',null,['class' => 'form-control','foo' => 'bar','size' => '30x5']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority','Priority:') !!}
    {!! Form::select('priority',['A'=>'A','B'=>'B','C'=>'C'],'A',['class' => 'form-control','multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText,['class' => 'blackbutton']) !!}
</div>

@section('footer')
@endsection