@extends('app')

@section('content')
    {!! Form::model($goal, ['method' => 'PATCH','url' => 'goals/'.$goal->id]) !!}

    @include('goals.form',['submitButtonText'=>'Update'])

    {!! Form::close() !!}

    @include('errors.list')
@stop