@extends('app')
@section('header')
    @include('partials.header')
@stop
@section('content')
    <table id="sortable" style="float: left; margin-right: 10px">
        @foreach($goals as $goal)
            @if(!$goal->iscompleted)
                <tr class="goal">
                    <td onclick="document.location.href = '/goals/{{$goal->id}}'">
                        @include('partials.statusandname',['item' => $goal])
                    </td>
                    <td class="actions">
                        @if(!$goal->iscompleted)
                            @include('buttons.complete',['url' => "/goals/$goal->id/complete", 'tooltip'=>'Complete goal' ])
                        @else
                            @include('buttons.uncomplete',['url'=>"/goals/$goal->id/uncomplete",'tooltip'=>'Uncomplete goal'])
                        @endif
                        <div class="btn-group">
                            <button type="button" class="blue" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                <span class="glyphicon glyphicon-menu-hamburger"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="/goals/{{$goal->id}}/edit">Edit</a></li>
                                <li><a href="/tasks/create/{{$goal->id}}">Add task</a></li>
                                <li style="cursor: pointer"><a onclick="
                                            var y = confirm('Are you sure you want to delete?');
                                            if (y == true) ajaxActionDelete('/goals/{{$goal->id}}','/goals/');
                                            ">Delete</a></li>
                            </ul>
                        </div>
                    </td>

                </tr>
            @endif

        @endforeach
    </table>
    <div class="description">
        <p>Create your long term goals. </p>

        <p>Goal can be a project, some target, achievment.</p>

        <p>It's important to set goals, so you know which task is for which goal.</p>
        @include('buttons.button',['text' => 'Create new goal','url'=>'/goals/create','class' => 'bluebutton'])
        {{--{!! Form::select('goals',$goals->lists('name','id'),null,['style' => 'text-transform:uppercase; margin-left: 10px;']) !!}--}}
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop