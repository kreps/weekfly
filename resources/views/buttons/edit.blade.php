<span
        onclick="document.location.href = '{{$url}}'"
        class="glyphicon glyphicon-edit"
        {{$ttp}}
        data-toggle="tooltip"
        title="{{$tooltip}}"
        aria-hidden="true">
</span>
