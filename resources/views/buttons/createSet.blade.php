<span
        onclick="document.location.href = '{{$url}}'"
        class="glyphicon glyphicon-plus btn btn-xs btn-default" aria-hidden="true" {{$ttp}}
        data-toggle="tooltip"
        title="{{$tooltip}}"></span>
