<span class="glyphicon glyphicon-plus-sign"
      aria-hidden="true"
      onclick="document.location.href = '{{$url}}'"
      {{$ttp}}
      data-toggle="tooltip"
      title="{{$tooltip}}"></span>
