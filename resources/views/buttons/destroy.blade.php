<span
        class="glyphicon glyphicon-remove"
        aria-hidden="true"
        onclick="
                var y = confirm('Are you sure you want to delete it?');
                if (y == true) {
                ajaxActionDelete('{{$url}}','{{$redirect}}');
                }
                "
        type="button"
        {{$ttp}}
        data-toggle="tooltip"
        title="{{$tooltip}}">
</span>
