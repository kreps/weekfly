<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" id="meta" name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Weekfly</title>
    <link rel="stylesheet" href="/css/all.css">
</head>
<body style="padding-top: 50px">
<div id="overlay">
    this is above the body!
</div>
@include('partials.nav')
<div class="container body">
    @yield('header')
    @yield('content')
</div>
@yield('footer')
<script src="/js/all.js"></script>
</body>
</html>
