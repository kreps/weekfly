@extends('app')

@section('content')

    {!! Form::open() !!}

    <div class="form-group">
        {!! Form::label('email','E-mail:') !!}
        {!! Form::input('email','email',null,['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password','Password:') !!}
        {!! Form::input('password','password',null,['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('rem','Remeber:') !!}
        {!! Form::checkbox('remember') !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Login',['class' => 'blackbutton']) !!}

    </div>

    {!! Form::close() !!}
    @include('buttons.button',['text' => 'Forgot password?','url'=>'/password/email','class' => 'bluebutton'])
    {{--<div class="container-fluid" id="login">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-body">--}}


    {{--<form role="form" method="POST" action="/auth/login">--}}
    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

    {{--<div class="form-group">--}}
    {{--<label class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="email" class="form-control" name="email" value="{{ old('email') }}">--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="password" class="form-control" name="password">--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<div class="checkbox">--}}
    {{--<label>--}}
    {{--<input type="checkbox" name="remember"> Remember Me--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}



    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

@endsection
