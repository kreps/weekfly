<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('goal_id')->nullable();
            $table->foreign('goal_id')->references('id')->on('goals')->onDelete('cascade');
            $table->string('name');
            $table->string('description')->nullable();
            $table->date('duedate')->nullable();
            $table->enum('priority', ['A', 'B', 'C'])->default('A');
            $table->unsignedInteger('duration')->nullable()->default(null);
            $table->boolean('iscompleted')->nullable();
            $table->unsignedInteger('sort');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
