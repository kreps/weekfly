<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->delete();
        $d = Carbon\Carbon::now();

        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,description) values (?, ?, ?,?,?,?,?)',
            [1, 'Weekfly project', $d, $d, $d, false, 'Simple yet powerful task planner with unlimited subtasks']);

        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,description) values (?, ?, ?,?,?,?,?)',
            [5, 'User login', $d, $d, $d, false, 'Add user login and authentication, tasks to user']);

        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id) values (?, ?, ?,?,?,?,?)', [7, 'Show all tasks', $d, $d, $d, true, 1]);
        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id) values (?, ?, ?,?,?,?,?)',
            [4, 'Parent id support in db', $d, $d, $d, true, 1]);

        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id) values (?, ?, ?,?,?,?,?)', [6, 'Add bootsrap style', $d, $d, $d, true, 1]);
        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id) values (?, ?, ?,?,?,?,?)', [9, 'sub sub task', $d, $d, $d, true, 6]);
        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id) values (?, ?, ?,?,?,?,?)', [8, 'Show single task view, with children', $d, $d, $d, true]);

        DB::insert('insert into tasks (id, name,duedate,created_at,updated_at,iscompleted,parent_id,description) values (?, ?, ?,?,?,?,?,?)',
            [3, 'Add task create form', $d, $d, $d, false,1,'Create form for adding goals and subtasks']);
    }
}
