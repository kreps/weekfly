var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss', 'resources/assets/css');

    mix.styles([
        //'libs/bootstrap-toggle.css',
        'libs/bootstrap.css',
        'app.css'
    ]);

    mix.scripts([
        'libs/jquery.js',
        'libs/jquery-blockui.js',
        //'libs/jquery.countdown.js',
        //'libs/bootstrap-toggle.js',
        'libs/bootstrap.js',
        'libs/jquery-ui/jquery-ui.js',
        'app.js'

    ]);
});
