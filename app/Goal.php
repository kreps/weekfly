<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $fillable = ['name', 'priority', 'sort', 'iscompleted', 'description'];

    public function tasks()
    {
        return $this->hasMany('App\Task', 'goal_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function setComplete()
    {
        $this->iscompleted = true;
        return $this;
    }

    public function setUncomplete()
    {
        $this->iscompleted = false;
        return $this;
    }
}
