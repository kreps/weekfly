<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubTaskRequest;
use App\Set;
use App\SubTask;
use App\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class SubTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id, SubTask $subtask = null)
    {
        return view('subtasks.create', compact('id', 'subtask'));
    }

    public function store(Request $request)
    {
        $subTask = new SubTask($request->all());
        $subTask->duration = stringToSeconds($subTask->duration);
        $subTask->save();
        return redirect('/tasks/' . $subTask->task_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function edit(SubTask $subtask)
    {
        if (Auth::user()->id == $subtask->user_id) {
            $id = $subtask->task_id;
            return view('subtasks.edit', compact('subtask', 'id'));
        } else {
            return redirect('tasks/');
        }
    }

    public function update(SubTask $subtask, SubTaskRequest $request)
    {
        $request['duration'] = stringToSeconds($request['duration']);
        $subtask->update($request->all());
        return redirect('tasks/' . $subtask->task_id);
    }

    public function destroy(SubTask $sub)
    {
        SubTask::destroy($sub->id);
        return Response::json(array('msg' => 'success', 'id' => $sub->id));
    }

    public function complete(SubTask $subtask)
    {
        $subtask->setComplete()->save();
        return Response::json(array('msg' => 'test', 'id' => $subtask->id, 'iscompleted' => true));
    }

    public function sort($sortedids)
    {
        $ids = explode(',',$sortedids);
        $sort = 0;
        foreach ($ids as $id){
            $task = SubTask::find($id);
            $task->sort = $sort++;
            $task->save();
        }
        return Response::json(array('ids' => $sortedids));
    }
}
