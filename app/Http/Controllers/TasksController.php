<?php

namespace App\Http\Controllers;

use App\Goal;
use App\Http\Requests;
use App\Task;
use App\Http\Requests\TaskRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        View::share('ttp', 'data-placement=left');
    }

    public function index()
    {
        $tasks = Auth::user()->tasks()->orderBy('sort', 'asc')->get();
        if (count(Auth::user()->goals())) {
            return view('tasks.index', compact('tasks'));
        }
        return redirect('goals/');
    }

    public function create($id = null)
    {
        $goals = Auth::user()->goals()->lists('name', 'id');
        $task = null;
        return view('tasks.create', compact('goals', 'task', 'id'));
    }

    public function store(TaskRequest $request)
    {
        $request['duration'] = stringToSeconds($request['duration']);
        Auth::user()->tasks()->create($request->all());
        return redirect('tasks/');
    }

    public function show(Task $task)
    {
        if (Auth::user()->id == $task->user_id) {
            $goal = Goal::find($task->goal_id);
            $subtasks = $task->subTasks()->where('iscompleted', '=', null)->orderBy('sort','asc')->get();
            return view('tasks.show', compact('task', 'subtasks', 'goal'));
        } else {
            return redirect('tasks/');
        }
    }

    public function edit(Task $task)
    {
        if (Auth::user()->id == $task->user_id) {
            $goals = Auth::user()->goals()->lists('name', 'id');
            $id = $task->goal_id;
            return view('tasks.edit', compact('goals', 'task', 'id'));
        } else {
            return redirect('tasks/');
        }
    }

    public function update(Task $task, TaskRequest $request)
    {
        $request['duration'] = stringToSeconds($request['duration']);
        $task->update($request->all());
        return redirect('tasks/' . $task->id);
    }

    public function destroy(Task $task)
    {
        Task::destroy($task->id);
        return Response::json(array('msg' => 'success', 'id' => $task->id, 'iscompleted' => true));
    }


    public function complete(Task $task)
    {
        $task->setComplete()->save();
        return Response::json(array('msg' => 'test', 'id' => $task->id, 'iscompleted' => true));
    }

    public function uncomplete(Task $task)
    {
        $task->setUncomplete()->save();
        return Response::json(array('msg' => 'test', 'id' => $task->id, 'iscompleted' => false));
    }

    public function duplicate(Task $task)
    {
        $task->load('subtasks');
        $task_new = $task->replicate();
        $task_new->name .= ' copy';
        $task_new->save();
        foreach ($task->subtasks as $sub) {
            $subnew = $sub->replicate();
            $subnew->task_id = $task_new->id;
            $subnew->save();
        }
        return redirect('tasks/');
    }

    public function sort($sortedids)
    {
        $ids = explode(',',$sortedids);
        $sort = 0;
        foreach ($ids as $id){
            $task = Task::find($id);
            $task->sort = $sort++;
            $task->save();
        }
        return Response::json(array('ids' => $sortedids));
    }
}
