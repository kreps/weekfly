<?php

namespace App\Http\Controllers;

use App\Goal;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use App\Http\Requests\GoalRequest;

class GoalsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        View::share('ttp', 'data-placement=left');
    }

    public function index()
    {
        $goals = Auth::user()->goals()->orderBy('priority', 'ASC')->orderBy('sort', 'ASC')->get();
        return view('goals.index', ['goals' => $goals]);
    }

    public function create()
    {
        return view('goals.create');
    }

    public function store(Request $request)
    {
        $goal = Auth::user()->goals()->create($request->all());
        return redirect('/goals/');
    }

    public function show(Goal $goal)
    {
        if ($goal->user_id != Auth::user()->id) {
            return redirect('/goals/');
        }
        $tasks = $goal->tasks()->where('iscompleted', '=', null)->get();
        return view('goals.show', compact('goal', 'tasks'));
    }

    public function edit(Goal $goal)
    {
        if ($goal->user_id != Auth::user()->id) {
            return redirect('/goals/');
        }
        return view('goals.edit', compact('goal'));
    }

    public function update(Goal $goal, GoalRequest $request)
    {
        if ($goal->user_id != Auth::user()->id) {
            return redirect('/goals/');
        }
        $goal->update($request->all());
        return redirect('/goals/' . $goal->id);
    }

    public function destroy(Goal $goal)
    {
        if ($goal->user_id != Auth::user()->id) {
            return Response::json(array('msg' => 'failed', 'id' => $goal->id));
        }
        Goal::destroy($goal->id);
        return Response::json(array('msg' => 'success', 'id' => $goal->id));
    }

    public function complete($id)
    {
        $goal = Goal::findOrFail($id);
        if ($goal->user_id != Auth::user()->id) {
            return Response::json(array('msg' => 'failed', 'id' => $goal->id));
        }
        $goal->setComplete()->save();
        return Response::json(array('msg' => 'Goal completed', 'goal' => $goal));
    }

    public function uncomplete($id)
    {
        $goal = Goal::findOrFail($id);
        if ($goal->user_id != Auth::user()->id) {
            return Response::json(array('msg' => 'failed', 'id' => $goal->id));
        }
        $goal->setUncomplete()->save();
        return Response::json(array('msg' => 'Goal opened', 'goal' => $goal));
    }

}
