<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::resource('goals', 'GoalsController');
Route::get('goals/{goal_id}/complete', ['as' => 'goals.complete', 'uses' => 'GoalsController@complete']);
Route::get('goals/{goal_id}/uncomplete', ['as' => 'goals.uncomplete', 'uses' => 'GoalsController@uncomplete']);

Route::resource('subtasks', 'SubTasksController');
Route::get('subtasks/create/{task_id?}', ['as' => 'subtasks.create', 'uses' => 'SubTasksController@create']);
Route::get('subtasks/{subtasks}/complete', ['as' => 'subtasks.complete', 'uses' => 'SubTasksController@complete']);
Route::get('subtasks/sort/{sortedids}', ['as' => 'subtasks.sort', 'uses' => 'SubTasksController@sort']);


Route::resource('tasks', 'TasksController');
Route::get('tasks/create/{goal_id?}', ['as' => 'tasks.create', 'uses' => 'TasksController@create']);
Route::get('tasks/{tasks}/complete', ['as' => 'tasks.complete', 'uses' => 'TasksController@complete']);
Route::get('tasks/{tasks}/uncomplete', ['as' => 'tasks.uncomplete', 'uses' => 'TasksController@uncomplete']);
Route::get('tasks/{tasks}/duplicate', ['as' => 'tasks.duplicate', 'uses' => 'TasksController@duplicate']);
Route::get('tasks/sort/{sortedids}', ['as' => 'tasks.sort', 'uses' => 'TasksController@sort']);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::get('/', function () {
    if (Auth::check())
        return Redirect::to('/tasks');
    return view('start');
});

Route::get('log', function () {
    return '@todo Log';
});

Route::get('schedule', function () {
    return '@todo Schedule';
});

Route::get('home', function () {
    return view('home');
});