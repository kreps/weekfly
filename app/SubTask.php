<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTask extends Task
{
    protected $fillable = ['name', 'iscompleted', 'description', 'task_id', 'duedate', 'priority', 'duration', 'sort'];

    public function task()
    {
        return $this->belongsTo('App/Task');
    }

    public function setComplete()
    {
        $this->iscompleted = true;
        return $this;
    }
}
