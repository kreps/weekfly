<?php

function secondsToTime($seconds)
{
    $seconds == null && $seconds = '0';
    $dtF = new DateTime("@0");
    $dtT = new DateTime("@$seconds");
    $dF = $dtF->diff($dtT);
    if ($dF->days) return "$dF->days day" . ($dF->days > 1 ? 's' : '');
    if ($dF->h) return "$dF->h hour" . ($dF->h > 1 ? 's' : '');
    if ($dF->i) return "$dF->i minute" . ($dF->i > 1 ? 's' : '');
    if ($dF->s) return "$dF->s second" . ($dF->s > 1 ? 's' : '');
    return '';
}

function stringToSeconds($str)
{
    if (!$str) {
        return 0;
    }
    if (!strstr($str, ' ')) {
        $str .= ' s';
    }
    list($num, $unit) = explode(' ', strtolower($str));

    if ($unit[0] == 's') {
        return $num;
    }
    if ($unit[0] == 'm') {
        return $num * 60;
    }
    if ($unit[0] == 'h') {
        return $num * 60 * 60;
    }
    if ($unit[0] == 'd') {
        return $num * 60 * 60 * 24;
    }
    return $num;
}

function nameShort($string){
    if (strlen($string) > 30){
        return substr($string,0,30).'...';
    } else {
        return $string;
    }
}