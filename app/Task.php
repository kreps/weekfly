<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $dates = ['duedate'];

    protected $fillable = ['name', 'iscompleted', 'description', 'goal_id', 'duedate', 'priority', 'duration', 'sort'];

    public function setDuedateAttribute($date)
    {
        $this->attributes['duedate'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public function getDuedateAttribute($date)
    {
        return new Carbon($date);
    }

    public function goal()
    {
        return $this->belongsTo('App\Goal');
    }

    public function setComplete()
    {
        $this->iscompleted = true;
        return $this;
    }

    public function setUncomplete()
    {
        $this->iscompleted = false;
        return $this;
    }

    public function subtasks()
    {
        return $this->hasMany('App\SubTask', 'task_id');
    }
}
