<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {


        $goal = factory(App\Goal::class)->make();
        $this->visit('/')
            ->see('Login')
            ->see('Create')
            ->see('account')
            ->click('Login')
            ->type('kristjan.kangro.78@gmail.com', 'email')
            ->type('123qwe', 'password')->press('Login')
            /*->see('goal')*/
            ->click('new goal')
            ->see('Name:')
            ->type('Test for unit test', 'name')
            ->visit('/goals' . $goal->id)/*->press('Create')*/
        ;


    }
}
